
const {BookingService} = require('../services/index');
const {StatusCodes} = require('http-status-codes');
const { createChannel, publishMessage } = require('../utils/messageQueue');
const { REMINDER_BINDING_KEY } = require('../config/serverConfig');

const bookingService = new BookingService();

class BookingController {

    constructor() {

    }

    async sendMessageToQueue (req, res) {
        try {
            console.log("Control reached here");
            const channel = await createChannel();
            const payload = {
                data: {
                    subject: 'This is a nofi from queue',
                    content: 'Some queue will subscribe this',
                    recepientEmail: 'ramsaicrony@gmail.com',
                    notificationTime: '2023-01-08 09:49:00'
                },
                service: 'CREATE_TICKET'
            };
            publishMessage(channel, REMINDER_BINDING_KEY, JSON.stringify(payload));
            return res.status(200).json({
                message: 'Successfully published the event',
            });
        } catch (error) {
            return res.status(error.statusCode).json({
                success: false,
                message: error.message,
                data: {},
                err: error.explanation
            });
        }
    }

    async create (req, res) {
        try {
            const response = await bookingService.createBooking(req.body);
            return res.status(StatusCodes.OK).json({
                success: true,
                message: 'Successfully completed booking',
                data: response,
                err: {}
            });
        } catch (error) {
            return res.status(error.statusCode).json({
                success: false,
                message: error.message,
                data: {},
                err: error.explanation
            });
        }
    }
}


module.exports = BookingController;