const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const { PORT } = require('./config/serverConfig');
const apiRoutes = require('./routes/index');
const db = require('./models/index');
// const router = express.Router();

const prepareAndStartServer = async () => {

    // keeping body-parser as middleware
    app.use(bodyParser.json());
    // helps us to read the request body properly
    app.use(bodyParser.urlencoded({extended: true}));

    // app.get('/bookingservice/api/v1/home', (req, res) => {
    //     return res.json({message: 'Hitting the booking service'});
    // })

    app.use('/bookingservice/api', apiRoutes);
    

    app.listen(PORT, () => {
        console.log(`Server started on port ${PORT}`);

        if(process.env.DB_SYNC) {
            db.sequelize.sync({alter: true});
        }
    });
}

prepareAndStartServer();